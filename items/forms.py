from django import forms

from .models import BuyItem

class BuyItemCreateForm(forms.ModelForm):
    class Meta:
        model = BuyItem
        fields = [
            "name",
            "quantity",
            "comment",
        ]
