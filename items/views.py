from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from .models import BuyItem
from .forms import BuyItemCreateForm


# Create your views here.
class BuyItemsListView(ListView):
    model = BuyItem
    def get_queryset(self):
        query = self.request.GET
        if query:
            if query.get('az'):
                return BuyItem.objects.order_by('name')
            if query.get('za'):
                return BuyItem.objects.order_by('-name')
            if query.get('begins'):
                return BuyItem.objects.filter(name__icontains=query.get('begins').capitalize())
        else:
            return BuyItem.objects.all()


class BuyItemsCreateView(CreateView):
    template_name = 'form.html'
    form_class = BuyItemCreateForm
    success_url = '/'

class BuyItemsUpdateView(UpdateView):
    form_class = BuyItemCreateForm
    success_url = '/'

    def get_queryset(self):
        return BuyItem.objects.filter(slug=self.kwargs['slug'])


class BuyItemsDeleteView(DeleteView):
    model = BuyItem
    success_url = '/'
    def get_queryset(self):
        return BuyItem.objects.filter(slug=self.kwargs['slug'])