from django.db import models
from django.db.models.signals import pre_save
from .utils import unique_slug_generator
from django.urls import reverse

# Create your models here.
class BuyItem(models.Model):
    name = models.CharField(max_length=50, blank=False, verbose_name=u"Покупка")
    quantity = models.CharField(max_length=15, blank=False, verbose_name=u"Количество")
    comment = models.CharField(max_length=30, blank=True, verbose_name=u"Комментарий")
    timestamp = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField(null=True, blank=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('edit', kwargs={"slug":self.slug})

def rl_pre_save_receiver(sender, instance, *args, **kwargs):

    if not instance.slug:
        instance.slug = unique_slug_generator(instance)
        instance.save()

pre_save.connect(rl_pre_save_receiver, sender=BuyItem)